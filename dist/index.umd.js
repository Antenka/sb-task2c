(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('express')) :
  typeof define === 'function' && define.amd ? define(['express'], factory) :
  (factory(global.express));
}(this, (function (express) { 'use strict';

express = 'default' in express ? express['default'] : express;

var app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task2A', function (req, res) {
  var a = +req.query.a || 0;
  var b = +req.query.b || 0;

  res.send('' + (a + b));
});

app.post('/task2A', function (req, res) {
  var a = +req.query.a || 0;
  var b = +req.query.b || 0;

  res.send('' + (a + b));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

})));
//# sourceMappingURL=index.umd.js.map
