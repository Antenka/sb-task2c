'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var express = _interopDefault(require('express'));

var app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task2A', function (req, res) {
  var a = +req.query.a || 0;
  var b = +req.query.b || 0;

  res.send('' + (a + b));
});

app.post('/task2A', function (req, res) {
  var a = +req.query.a || 0;
  var b = +req.query.b || 0;

  res.send('' + (a + b));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
//# sourceMappingURL=index.js.map
