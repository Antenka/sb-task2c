import express from 'express';

import canonize from './canonize';

const app = express();


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task2C', (req, res) => {
  return res.send(canonize(req.query.username));
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
