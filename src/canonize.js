export default function canonize(url){
  const re = new RegExp('@?(https?:)?(\/\/)?((www.)?[a-zA-Z0-9.-]*/)?@?([a-zA-Z0-9._]*)(.)?', 'i');
  const userName = url.match(re)[5];
  return '@' + userName;
}
